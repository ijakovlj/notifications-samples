# Script examples to integrate with CERN Notifications service

A list of different scripts to interact with the CERN Notifications service can be found here.
Disclaimer: these scripts are provided as a kick-start and helper

### Examples

- send-notification: sending a notification via the REST API. Using the API Key.
- rss-to-notifications: sync RSS feed(s) to a Notification Channel via the API. Using the API Key.
- get-api-token: get an OAuth access_token to access the full backend API (different from the API Key which has only send possibility)

### Contribute

If you have worked on other integration scripts or implemented the current examples in different languages please share them with us via a Merge Request.

# send-notification script

- Fill ```channelId``` and ```notifications_bearer``` from Notification service information

Use corresponding base uri depending on the target infrastructure:
- https://notifications.web.cern.ch/api
- https://notifications-qa.web.cern.ch/api
- https://notifications-dev.web.cern.ch/api

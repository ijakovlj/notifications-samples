const https = require('https');

// Target Notification channel
const notifications_sendapi = "https://notifications.web.cern.ch/api/notifications";
const notifications_bearer = "fill-me with Channel API Key";
const channelId = "fill-me with ChannelID";

// Notification json sample
// {
//  "notification": {
//    "target": "...",
//    "summary": "Notification title",
//    "priority": "LOW|NORMAL|IMPORTANT",
//    "body": "Notification content",
//     "link": if any,
//     "imgurl": if any,
//  }
// }

// Build json and post to notifications service
const message = JSON.stringify({
    "notification": {
        "body": "<p>Message content</p>",
        "summary": "This is a test notification",
        "target": channelId,
        "priority": 'NORMAL',
        "link": "any alternate url you might want to provide",
    }
});

const options = {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': message.length,
        'Authorization': 'Bearer ' + notifications_bearer,
    }
};

const req = https.request(notifications_sendapi, options, res => {
    console.log(`statusCode: ${res.statusCode}`);

    res.on('data', d => {
        process.stdout.write(d);
    })
})

req.on('error', error => {
    console.error(error);
})

req.write(message);
req.end();
# get-api-token Python script

- Needs https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie/
    - Works on LXPLUS for example
- Edit ```clientapp_name``` and ```audience``` depending on your target (dev, qa, prod)

Sample:
```
ACCESS_TOKEN=$(python get-api-token.py)
curl -X GET "https://api-notifications-dev.app.cern.ch/channels/" -H  "authorization: Bearer $ACCESS_TOKEN"
curl -X GET "https://api-notifications-dev.app.cern.ch/preferences/" -H  "authorization: Bearer $ACCESS_TOKEN"
curl -X GET "https://api-notifications-dev.app.cern.ch/devices/" -H  "authorization: Bearer $ACCESS_TOKEN"
```

import unittest
from unittest.mock import MagicMock, patch
from notification_sender import NotificationSender, Priority

class TestNotificationSender(unittest.TestCase):
    
    def setUp(self):
        self.ns = NotificationSender()
        self.ns.CHANNEL_ID = 'test_channel'
        self.ns.NOTIFICATIONS_SENDAPI = 'http://test_api'
        self.ns.NOTIFICATIONS_BEARER = 'test_bearer'
        self.ns.NOTIFICATION_TITLE = 'test_title'
        self.ns.PRIORITY = 'LOW'
        
    def test_check_configuration_missing_params(self):
        self.ns.NOTIFICATION_TITLE = None
        self.assertEqual(self.ns._check_configuration(), ['NOTIFICATION_TITLE'])
        
    def test_check_configuration_no_missing_params(self):
        self.assertEqual(self.ns._check_configuration(), [])
        
    def test_send_notification(self):
        notification_text = 'test_notification_text'
        summary = 'test_notification_summary'
        priority = Priority.NORMAL
        link = 'test_link'
        image_url = 'test_image_url'
        expected_url = self.ns.NOTIFICATIONS_SENDAPI
        expected_headers = {'Authorization': 'Bearer ' + self.ns.NOTIFICATIONS_BEARER}
        expected_data = {
            'body': notification_text,
            'summary': summary,
            'target': self.ns.CHANNEL_ID,
            'priority': priority.name,
            'link': link,
            'imgUrl': image_url
        }
        mock_response = MagicMock()
        with patch('requests.post', return_value=mock_response) as mock_post:
            response = self.ns._send_notification(notification_text, summary, priority, link, image_url)
            mock_post.assert_called_once_with(expected_url, json=expected_data, headers=expected_headers)
            self.assertEqual(response, mock_response)
        
    def test_run(self):
        with patch('notification_sender.NotificationSender._send_notification', return_value='mock_response') as mock_send_notification:
            self.assertEqual(self.ns.run(), 'mock_response')
            mock_send_notification.assert_called_once()
# Reccurent Notifications

NotificationSender is a Python class that provides an easy way to send messages to a specific Notifications channel at a scheduled time. It uses the Schedule module to execute a job at a specific time and the Requests module to send HTTP requests to the Notifications API.

## Installation

The NotificationSender class requires the following Python packages to be installed:

- [requests](https://pypi.org/project/requests/)
- [schedule](https://pypi.org/project/schedule/)

These packages can be installed using pip, the Python package manager, by running the following command:

```python
pip install requests schedule
```

or

```python
pip install -r requirements.txt
```

## Configuration

NotificationSender requires the following environment variables to be set:

- `CHANNEL_ID`: the ID of the Notifications channel to which the notification will be sent.
- `NOTIFICATIONS_SENDAPI`: the URL of the Notifications API to send notifications to.
- `NOTIFICATIONS_BEARER`: the bearer token used to authenticate with the Notifications API.
- `NOTIFICATION_TITLE`: the title of the notification.
- `PRIORITY`: the priority of the notification, either "LOW" or "NORMAL".
- `LINK` (optional): the URL to which the notification should link.
- `IMAGE_URL` (optional): the URL of an image to include in the notification.

NotificationSender also supports reading configuration from a `.env` file in the current working directory. You can create a `.env` file and set the required environment variables as key-value pairs:

```bash
CHANNEL_ID=<channel_id>
NOTIFICATIONS_SENDAPI=<notifications_api_url>
NOTIFICATIONS_BEARER=<bearer_token>
NOTIFICATION_TITLE=<notification_title>
PRIORITY=<priority>
LINK=<link_url>
IMAGE_URL=<image_url>
```

## How to create/edit the notification text

The `notification_text.html` file is a local file that contains the body of the notification message that will be sent by the `NotificationSender` class. The `NotificationSender.run()` method reads the contents of this file and uses it as the `notification_text` argument when calling the `_send_notification()` method to send the notification message.

The content of the file can be written in HTML format, allowing for styling of the notification message body.

## Usage

To use NotificationSender, you need to create an instance of the class and set the required environment variables for your Notifications API credentials, channel ID, and other configuration parameters.

```python
from NotificationSender import NotificationSender

notification_sender = NotificationSender()
notification_sender.run()

```

## Run Test Cases

```bash
 python3 -m unittest discover tests
```

# Notification Sender Schedule

The code in ``schedule_job.py`` demonstrates how to use the schedule library to schedule a job to run at specific times. The example provided in this code schedules the `NotificationSender` class to run at `11:00 AM every workday`.

## How to Use

To use this code, follow the steps below:

1. Import the required libraries: `schedule`, `time`, and `NotificationSender`.
2. Create an instance of the `NotificationSender` class.
3. Use the `schedule.every()` method to schedule a job at the desired time. In this example, the job is scheduled for `11:00 AM every workday` using the `.monday.at()`, `.tuesday.at()`, `.wednesday.at()`, `.thursday.at()`, and `.friday.at()` methods.
4. Run the scheduled jobs using the `schedule.run_pending()` method.
5. Sleep for 1 second using the `time.sleep(1)` method to avoid overloading the system.

## Simple Example Code

Here's an example code to schedule notifications `every Monday at 12 AM` using the schedule module and the `NotificationSender` class:

```python
import schedule
import time
from notification_sender import NotificationSender

if __name__ == '__main__':
    ns = NotificationSender()

    # Schedule notifications every Monday at 12 AM
    schedule.every().monday.at("00:00").do(ns.run)

    while True:
        schedule.run_pending()
        time.sleep(1)
```

*Note that this code assumes that the required environment variables for the `NotificationSender` class are properly set.

## Example Times

The schedule library provides several methods for scheduling jobs. Some examples include:

```python
every().minute.do(job)
every().hour.do(job)
every().day.at("10:30").do(job)
every().monday.do(job)
every().wednesday.at("13:15").do(job)
every().minute.at(":17").do(job)
```

# Run as a CronJob on PAAS

To run the code as a CronJob in PAAS, it is necessary to do the following steps:

1. Use the `.gitlab-ci.yaml` to build a custom docker image from the project via [gitlab.cern.ch](https://gitlab.cern.ch)
2. Modify the `/paas/deployment.yaml` and `/paas/configmap.yaml` files to include all necessary configuration parameters
3. Execute `oc create configmap <CUSTON_CONFIGMAP_NAME> --from-file=paas/configmap.yaml`
4. Execute `oc create -f paas/cronjob.yaml`

> The CronJob should be deployed to the PAAS infrastructure

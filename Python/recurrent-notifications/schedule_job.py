import schedule
import time
from notification_sender import NotificationSender

if __name__ == '__main__':
    ns = NotificationSender()
    
    schedule.every().monday.at("11:00").do(ns.run)
    schedule.every().tuesday.at('11:00').do(ns.run)
    schedule.every().wednesday.at('11:00').do(ns.run)
    schedule.every().thursday.at('11:00').do(ns.run)
    schedule.every().friday.at('11:00').do(ns.run)

    while True:
        schedule.run_pending()
        time.sleep(1)
import enum
import os
import requests


class Priority(enum.Enum):
    LOW = 1
    NORMAL = 2


REQUIRED_ENV_VARS = ['CHANNEL_ID', 'NOTIFICATIONS_SENDAPI',
                     'NOTIFICATIONS_BEARER', 'NOTIFICATION_TITLE', 'PRIORITY']
OPTIONAL_ENV_VARS = ['LINK', 'IMAGE_URL']
ENV_VARS = REQUIRED_ENV_VARS + OPTIONAL_ENV_VARS


class NotificationSender():

    def __init__(self) -> None:
        """
            Initializes NotificationSender object by setting environment variables for the object's instance attributes
        """
        self.__dict__.update((var, os.getenv(var)) for var in ENV_VARS)

    def _check_configuration(self):
        """
        Checks if all the required configuration parameters are set
        
        Returns:
            List: a list of missing configuration parameters, if any
        """
        return [attr for attr in REQUIRED_ENV_VARS if not getattr(self, attr, None)]

    def _send_notification(self: object, notification_text: str, notification_title: str, priority: Priority, link: str, image_url: str) -> requests.Response:
        """
        Sends a notification by using the Notification API endpoint and the configuration set in the NotificationSender object
        
        Args:
            notification_text (str): the notification text that will be sent
            notification_title (str): the title of the notification that will be sent
            priority (Priority): the priority level of the notification that will be sent
            link (str): a link that will be included in the notification, if provided
            image_url (str): an image url that will be included in the notification, if provided
        
        Returns:
            requests.Response: the response of the notification send request
        """

        header = {'Authorization': 'Bearer ' + self.NOTIFICATIONS_BEARER}
        message = {
            "body": notification_text,
            "summary": notification_title,
            "target": self.CHANNEL_ID,
            "priority": priority.name,
            "link": link,
            "imgUrl": image_url
        }
        print(message)
        return requests.post(self.NOTIFICATIONS_SENDAPI, json=message, headers=header)

    def run(self: object) -> None:
        """
        Sends a notification by calling the _send_notification method and using the configuration set in the NotificationSender object
        """
        missing_attributes = self._check_configuration()
        print(f'Sending Notification to {self.CHANNEL_ID}')
        if (missing_attributes):
            missing_attributes = ', '.join(missing_attributes)
            raise Exception(
                f'The following configuration parameters are not set: {missing_attributes}')

        with open('notification_text.html') as file:
            notification_text = file.read()

        summary = self.NOTIFICATION_TITLE
        priority = Priority[self.PRIORITY]
        link = self.LINK
        image_url = self.IMAGE_URL
        return self._send_notification(notification_text, summary, priority, link, image_url)

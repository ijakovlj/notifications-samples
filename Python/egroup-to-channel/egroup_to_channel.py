#!/usr/bin/python

import sys, getopt
from api_library.egroup_from_ldap import egroup_from_ldap
from api_library.egroups_api import addMemberEmail
from api_library.grappa_api import update_sync_type
from api_library.channel import create_channel, remove_me_from_channel, set_channel_owner, add_egroup_to_channel

def usage():
    print('egroup-to-channel.py -g <egroupname> [-a <admingroup> -o <owner> -d <description> --ldap --removeSync]')
    print('\t-g <egroupname> is required')
    print('\t--ldap will search in ldap for admingroup, owner and description information')
    print('\t--removeSync: stop sync between EGroup and Grappa, clear EGroup members. Either this or ask Mail Team to manually change the EGroup mail target.')

# Main
def main(argv):
    egroup = ''
    adminGroup = ''
    owner = ''
    description = ''
    enableldap = False
    removeSync = False
    try:
        opts, args = getopt.getopt(argv, "lhg:a:d:o:", ["group=", "admingroup=", "description=", "owner=", "ldap", "removeSync"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-g", "--group"):
            egroup = arg
        elif opt in ("-a", "--admingroup"):
            adminGroup = arg
        elif opt in ("-o", "--owner"):
            owner = arg
        elif opt in ("-d", "--description"):
            description = arg
        elif opt in ("-l", "--ldap"):
            enableldap = True
        elif opt in ("--removeSync"):
            removeSync = True
    if not egroup:
        usage()
        sys.exit(2)

    # Remove @cern.ch if any
    egroup = egroup.replace('@cern.ch', '')

    if enableldap:
        print("Retrieving egroup information from Xldap")
        ret = egroup_from_ldap(egroup)
        if ret:
            owner = ret['owner']
            if ret['adminGroup']:
                adminGroup = ret['adminGroup']
            description = ret['description']

    print('Creating Channel from Egroup')
    print('\tName: ', egroup)
    print('\tOwner: ', owner)
    print('\tAdmin group: ', adminGroup)
    print('\tDescription: ', description)

    if not owner:
        print("Missing owner username")
        usage()
        sys.exit(2)

    # Get bearer
    # ACCESS_TOKEN=get_api_token()
    # HEADER={'Authorization': 'Bearer ' + ACCESS_TOKEN}

    # Create Channel
    channel_id, channel_slug = create_channel(egroup, adminGroup, description)
    if not channel_id:
        print('Error creating channel ', egroup)
        sys.exit()
    # Add corresponding Grappa group to members
    add_egroup_to_channel(channel_id, egroup)
    # Remove me from members
    remove_me_from_channel(channel_id)
    # Change owner to egroup owner
    set_channel_owner(channel_id, owner)

    channel_email = "notifications+" + channel_slug + "+NORMAL@dovecotmta.cern.ch"

    if removeSync:
        print("Migrating with removeSync option: egroups will be cleaned and grappa sync disabled.")
        # Remove Egroup <-> Grappa sync for this Grappa group
        update_sync_type(egroup)
        # Clear Egroup members and set only the channel email as member
        print("Channel email:", channel_email)
        addMemberEmail(egroup, channel_email)
        print("Egroup was cleaned, kept only 1 member:", channel_email)
    else:
        print("Egroup mailing needs to be disabled by Mail Team, and forwarded to:", channel_email)

if __name__ == "__main__":
   main(sys.argv[1:])

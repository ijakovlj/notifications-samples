#
# script to create a new eGroup from a template using python's "suds" package
# (https://fedorahosted.org/suds/) I've been using 0.4.1(beta) successfully.
# The package needs to be installed and available in your PYTHONPATH or you need
# to set the path explicitly (see below)
#
# Author: Andreas Pfeiffer (andreas.pfeiffer@cern.ch) May 11, 2014
#
# latest update: Aug 27, 2014 : handle case where template group does not have
#                any members yet (code kindly provided by Joel.Closier@cern.ch)
#                works also with Python 2.7
#
import os, sys
#import logging
#logging.basicConfig(level=logging.INFO, filename='./suds.log')
#logging.getLogger('suds.client').setLevel(logging.DEBUG)
#logging.getLogger('suds.transport').setLevel(logging.DEBUG)
# for (much) more debugging uncomment the next two logging lines
# ---------------------------------------------------------------------
# CAREFUL: enabling these lines will show your connection details
#          in clear text on the screen (including your password) !!!!
# ---------------------------------------------------------------------
# logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)
# logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
# add the path where "suds" is installed (~/python/ in my case)
# if you have it installed in your system (and available in your PYTHONPATH,
# you can comment the next two lines:
sudsPath = os.path.join(os.environ['HOME'],'python')
sys.path.append(sudsPath)
from suds.client import Client #  pip3 install suds-py3
from suds.transport.http import HttpAuthenticated
from suds import WebFault
if sys.version_info < (3, 0):
    import urllib2
else:
    import urllib.request as urllib2
from getpass import getpass

# helper functions
def checkOK( replyIn ):
    reply = str( replyIn )
    if "ErrorType" in reply : return False
     
    return True
    
def findGroup(groupname = None):
    print("Enter credentials for egroups API access")
    login = input("Login: ") 
    pwd = getpass('Password: ')

    url = 'https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl'
    client = Client(url, username=login, password=pwd)
    group = client.service.FindEgroupByName(groupname).result
    # for more debugging:
    # print "client.service.FindEgroupByName returned:"
    # print groupTmpl
    # print "="*80
    return client, group
    
# add a member to an eGroup
def addMemberEmail(eGroupName, eMailAddress):
    client, group = findGroup(eGroupName)
    print("Adding member ", eMailAddress, 'to', eGroupName)

    members = []
    member = client.factory.create('ns0:MemberType')
    member.Email = eMailAddress
    member.Type = 'External'
    members.append(member)

    overWriteMembers = True    # or True, if you want to reset the list
    ret = client.service.AddEgroupMembers( eGroupName, overWriteMembers, members )
    if not checkOK( ret ):
        print("ERROR could not add user", eMailAddress, 'to group', eGroupName)
        print("      reason given by server:", ret)


# if __name__ == "__main__":
#    # for simplicity, just take the first argument as name of the new group:
#    newGroupName = sys.argv[1].lower()
#    # create the new group based on the template (see above)
#    #newGroup( newGroupName )
    
#    # now we have the group, so we can add a new member.  
#    addMemberEmail( newGroupName, 'notifications+another-test+NORMAL@dovecotmta.cern.ch' )
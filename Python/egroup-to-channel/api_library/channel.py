import requests
import re, sys
from api_library.get_api_token import get_api_token

BACKEND_URL='https://notifications-dev.web.cern.ch/api'
#BACKEND_URL='https://localhost:8080'
ACCESS_TOKEN=get_api_token()
HEADER={'Authorization': 'Bearer ' + ACCESS_TOKEN}
VERIFY=False # Verify SSL certificate for requests

# Create new Channel
def create_channel(egroup, admingroup, description):
    print('Creating Channel')
    data = {'channel': {
        'name': egroup,
        'slug': re.sub('[^0-9a-z-_]', '-', egroup.lower()),
        'description': description + ' - Migrated from Egroups',
        'adminGroup': { 'groupIdentifier': admingroup },
        'visibility': 'RESTRICTED',
        'submissionByForm': [ 'ADMINISTRATORS' ],
        'submissionByEmail': [ 'EGROUP' ],
        'incomingEgroup': egroup + '@cern.ch',
    }}
    #print(data)
    r = requests.post(BACKEND_URL + '/channels/', json=data, headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error creating channel', r.json())
        sys.exit(2)
    new_channel = r.json()
    #print(new_channel)

    return new_channel['id'], new_channel['slug']

# Add egroup as Channel Member
def add_egroup_to_channel(channel_id, egroup):
    print('Adding group to Channel members', egroup)
    data = { 'group': { 'groupIdentifier': egroup } }
    r = requests.put(BACKEND_URL + '/channels/' + channel_id + '/groups', json=data, headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error updating channel', r.json())
        sys.exit(2)
    updated_channel = r.json()

    return updated_channel['id']

# Remove ME from Members
def remove_me_from_channel(channel_id):
    print('Removing ME from Channel members')
    r = requests.get(BACKEND_URL + '/usersettings', headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error removing ME from channel', r.json())
        sys.exit(2)
    me = r.json()
    if not me['userId']:
        print('error retrieving ME', me)
        sys.exit(2)

    data = { 'userId': me['userId'] }
    r = requests.delete(BACKEND_URL + '/channels/' + channel_id + '/members', json=data, headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error removing ME from channel members', r.json())
        sys.exit(2)
    updated_channel = r.json()

    return updated_channel['id']

# Change Channel owner
def set_channel_owner(channel_id, username):
    print('Setting Channel owner to', username)
    data = { 'username': username }
    r = requests.put(BACKEND_URL + '/channels/' + channel_id + '/owner', json=data, headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error setting channel owner', r.json())
        sys.exit(2)
    updated_channel = r.json()

    return updated_channel['id']

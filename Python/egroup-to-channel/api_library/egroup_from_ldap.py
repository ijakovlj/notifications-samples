import ldap
import json

def egroup_from_ldap(egroup):
    l = ldap.initialize("ldap://xldap.cern.ch")
    try:
        l.protocol_version = ldap.VERSION3
        l.set_option(ldap.OPT_REFERRALS, 0)
    
        #bind = l.simple_bind_s("me@example.com", "password")
    
        base = "OU=Workgroups,DC=cern,DC=ch"
        criteria = "(&(objectClass=group)(sAMAccountName=" + egroup + "))"
        attributes = ['cn', 'description', 'managedBy', 'extensionAttribute6']
        result = l.search_s(base, ldap.SCOPE_SUBTREE, criteria, attributes)
    
        results = [entry for dn, entry in result if isinstance(entry, dict)]
        #print(results)

        return {
            'name': str(results[0]['cn'][0], 'utf-8'),
            'description': str(results[0]['description'][0], 'utf-8'),
            'adminGroup': str(results[0]['extensionAttribute6'][0], 'utf-8') if results[0].get('extensionAttribute6') else None,
            'owner': str(results[0]['managedBy'][0], 'utf-8').replace(',OU=Users,OU=Organic Units,DC=cern,DC=ch', '').replace('CN=', ''),
        }

    # except:
    #     return None
    finally:
        l.unbind()
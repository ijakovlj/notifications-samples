from api_library.grappa_get_api_token import grappa_get_api_token
import requests
import sys

def update_sync_type(group_id, sync_type="NoSync"):
    """
    Call the AuthZSvc API to update the sync type
    """
    GRAPPA_API_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"
    SYNC_TYPE_KEY = "syncType"
    #sync_types = ["Slave", "SlaveWithPlaceholders", "Master", "NoSync"]
    ACCESS_TOKEN=grappa_get_api_token()
    HEADER={'Authorization': 'Bearer ' + ACCESS_TOKEN}
    VERIFY=False # Verify SSL certificate for requests

    r = requests.get(GRAPPA_API_URL + '/Group/' + group_id, headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error retrieving group', r.json())
        sys.exit(2)
    group = r.json()
    #print('retrieved group', group)
    if not group['data']:
        print('error retrieving group', group)
        sys.exit(2)

    group = group['data']
    group[SYNC_TYPE_KEY] = sync_type

    r = requests.put(GRAPPA_API_URL + '/Group/' + group_id, json=group, headers=HEADER, verify=VERIFY)
    if r.status_code != requests.codes.ok:
        print('error updating group', r.json())
        sys.exit(2)
    updated_group = r.json()
    #print('updated_group', updated_group)

    return updated_group['data'][SYNC_TYPE_KEY] 


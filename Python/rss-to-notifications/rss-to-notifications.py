import feedparser
import requests
import os.path

# Array of source RSS Feeds URIs
FeedList = ["https://home.cern/api/news/feed.rss"]

# Datafile used to remember which posts were already synced
datafile = "rss-to-notifications.dat"

# Target Notification channel
notifications_sendapi = "https://notifications.web.cern.ch/api/notifications"
notifications_bearer  = "fill-me with Channel API Key"
channelId = "fill-me with ChannelID"

header = {'Authorization': 'Bearer ' + notifications_bearer}

for OneFeed in FeedList:
    Feed = feedparser.parse(OneFeed)
    #entry = Feed.entries[1]

    for msg in Feed.entries:

        # Extract item guid to check if already synced
        guid = msg.guid
        if not guid:
            print("Missing GUID skipping item or it will sync forever")
            continue

        alreadyposted = False
        if os.path.isfile(datafile):
            with open(datafile) as file:
                if any(line.rstrip('\n') == guid for line in file):
                    alreadyposted = True

        if not alreadyposted:
            print("Posting message ", guid)
            # Notification json sample
            # {
            #  "notification": {
            #    "target": "...",
            #    "summary": "Notification title",
            #    "priority": "LOW|NORMAL|IMPORTANT",
            #    "body": "Notification content",
            #     "link": if any,
            #     "imgurl": if any,
            #  }
            # }

            # One can try to extract a feature image and fill it's url in imgurl property
            # But the image must be accessible anonymously to render fine in the notification list

            # Build json and post to notifications service
            message = {
                "notification": {
                    "body": msg.description,
                    "summary": msg.title,
                    "target": channelId,
                    "priority": 'NORMAL',
                    "link": msg.link,
                }
            }
            #print(message)
            response = requests.post(notifications_sendapi, json=message, headers=header)
            #print(response)
            #print(response.json())

            # Saves which entries are posted
            file_object = open(datafile, 'a')
            file_object.write(guid + '\n')
            file_object.close()
        else:
            print("Already posted.")


# rss-to-notification Powershell script

- Edit ```$FeedList```, an array of RSS sources.
- Edit ```$datafile``` path
- Fill ```channelId``` and ```notifications_bearer``` from Notification service information
- In the loop, an old SSO automatic auth with kerberos is provided (commented out by default) 

Use corresponding base uri depending on the target infrastructure:
- https://notifications.web.cern.ch/api
- https://notifications-qa.web.cern.ch/api
- https://notifications-dev.web.cern.ch/api

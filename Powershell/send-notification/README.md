# send-notification Powershell scripts
- ```send-notification.ps1``` : sending a normal notification
- ```send-targeted-notification.ps1``` : sending a targeted notification
- Fill ```channelId``` and ```notifications_bearer``` from Notification service information
- Tip: if content encoding is utf8, don't forget ```-encoding utf8``` parameter on ```get-content```  or equivalent.

Use corresponding base uri depending on the target infrastructure:
- https://notifications.web.cern.ch/api
- https://notifications-qa.web.cern.ch/api
- https://notifications-dev.web.cern.ch/api

# Target Notification channel
$notifications_sendapi = "https://notifications.web.cern.ch/api/notifications"
$notifications_bearer  = "......."
$channelId = "......"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Targets array
$targetUsers = [System.Collections.ArrayList]::new()
$targetUsers.Add([pscustomobject]@{email = "john.doe@cern.ch" })

$Notification = @{ target=$channelId; summary='Title'; priority='NORMAL'; body='description'; targetUsers=$targetUsers }
$Payload = @{ notification=$Notification}
$headers = @{Authorization = "Bearer $notifications_bearer"}
# convertto-json needs -depth parameter in order to convert also the targetUsers array
Invoke-RestMethod -Uri $notifications_sendapi -Method Post -ContentType 'application/json;charset=utf-8' -Headers $headers -Body (ConvertTo-Json $Payload -Depth 10)
# Simple curl examples
Use corresponding base uri depending on the target infrastructure:
- https://notifications.web.cern.ch/api
- https://notifications-qa.web.cern.ch/api
- https://notifications-dev.web.cern.ch/api

## Basic Curl example:
```
curl -X POST https://notifications.web.cern.ch/api/notifications \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer <api-key>" \
  --insecure \
  --data '{
    "notification":
      {
        "target": 'channel id that you can find in your browser url bar'
        "summary":"Test notification",
        "priority":"NORMAL",
        "body":"<p>This is a test notification sent via the API</p>"
      }
  }'
```
## Curl example for a targeted notification:
```
curl -X POST https://notifications.web.cern.ch/api/notifications \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer <api-key>" \
  --insecure \
  --data '{
    "notification":
      {
        "target": 'channel id that you can find in your browser url bar'
        "summary":"Test targeted notification",
        "priority":"NORMAL",
        "body":"<p>This is a test targeted notification sent via the API</p>",
        "private": true,
        "targetUsers": [{"email": ""some-member@some.domain"}, ...],
        "targetGroups": [{"groupIdentifier": "some-group"}, ...],
        "targetData": ["some-member@some.domain", "some-group", ...]
      }
  }
```
Notes:
- ```targetUser```: can contain only user objects with an email address,
- ```targetGroups```: can contain only group objects with a groupIdentifier containing the name,
- ```targetData```: can contain mixed strings emails and group names (but degraded performance therefore not the recommended option).
